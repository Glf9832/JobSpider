#_*_ coding:utf-8 _*_

import requests
import json
import redis
import logging
import settings

logger = logging.getLogger(__name__)
##使用REDIS_URL链接Redis数据库, deconde_responses=True这个参数必须要，数据会变成byte形式 完全没法用
reds = redis.Redis.from_url(settings.REDIS_URL, db=2, decode_responses=True)
login_url = 'https://login.51job.com/login.php'

#获取Cookie
def get_cookie(account,password):
    s = requests.Session()
    payload = {
        # 'lang':'c',
        # 'action':'save',
        # 'from_domain':'i',
        'loginname':account.encode('utf-8'),
        'password':password.encode('utf-8'),
        # 'verifycode':'',
        # 'isread':'on'
    }
    response = s.post(login_url,data=payload)
    cookies = response.cookies.get_dict()
    logger.warning("获取Cookies成功！（账号为：%s）"%account.encode('utf-8'))
    return json.dumps(cookies)

def init_cookie(red,spidername):
    redkeys = reds.keys()
    for user in redkeys:
        password = reds.get(user)
        if red.get("%s:Cookies:%s--%s"%(spidername,user,password)) is None:
            cookie = get_cookie(user,password)
            red.set("%s:Cookies:%s--%s"%(spidername,user,password),cookie)

def update_cookie(red,accountText,spidername):
    red = redis.Redis()
    pass

def remove_cookie(red,spidername,accountText):
    #red = redis.Redis()
    red.delete("%s:Cookies:%s"%(spidername,accountText))
    pass
