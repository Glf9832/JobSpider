# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JobItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    position = scrapy.Field()
    area = scrapy.Field()
    wages = scrapy.Field()
    unit = scrapy.Field()
    companyName = scrapy.Field()
    nature = scrapy.Field()
    peonum = scrapy.Field()
    industry = scrapy.Field()
    askFor = scrapy.Field()
    treatment = scrapy.Field()
    minfo = scrapy.Field()
    contact = scrapy.Field()
    url = scrapy.Field()
    crawltime = scrapy.Field()