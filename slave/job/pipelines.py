# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from datetime import datetime
import settings
from items import JobItem

class JobPipeline(object):
    def __init__(self):
        client = pymongo.MongoClient(host=settings.Mongodb_Host,port=settings.Mongodb_Port)
        db = client["jobdb"]
        # nowtime = datetime.now().strftime('%Y%m%d%H%M%S')
        self.jobinfo = db["jobinfos"]

    def process_item(self, item, spider):
        if isinstance(item,JobItem):
            try:
                self.jobinfo.insert(dict(item))
            except Exception as e:
                print e
        return item
