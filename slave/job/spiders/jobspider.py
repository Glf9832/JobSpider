# -*- coding: utf-8 -*-
import scrapy
from job.items import JobItem
from scrapy.http import Request
from scrapy_redis.spiders import RedisSpider
import time
import re

class JobspiderSpider(RedisSpider):
    name = 'jobspider'
    allowed_domains = ['search.51job.com','jobs.51job.com']
    redis_key = "jobspider:start_urls"

    def parse(self,response):
        print u"解析页："+response.url
        item = JobItem()
        try:
            res = response.xpath('//div[@class="tCompany_center clearfix"]/div[@class="tHeader tHjob"]/div[@class="in"]/div[@class="cn"]')
            position = res[0].xpath('./h1/text()')
            area = res[0].xpath('./span[@class="lname"]/text()')
            wages = res[0].xpath('./strong/text()')
            companyName = res[0].xpath('./p[@class="cname"]/a/text()')
            nature = res[0].xpath('./p[@class="msg ltype"]/text()')
            head = response.xpath('//div[@class="tCompany_main"]/div[@class="tBorderTop_box bt"]/div[@class="jtag inbox"]')
            askFor = head[0].xpath('./div[@class="t1"]').xpath('string(.)')
            treatment = head[0].xpath('./p[@class="t2"]').xpath('string(.)')
            minfo = response.xpath('//div[@class="tCompany_main"]/div[2]').xpath('string(.)')
            contact = response.xpath('//div[@class="tCompany_main"]/div[3]').xpath('string(.)')
        except Exception as e:
            print e
        try:
            if position:
                item["position"] = position[0].extract()    #职位信息
            if area:
                item["area"] = area[0].extract()    #地区
            if wages:
                wagess = wages[0].extract().replace('\r','').replace('\n','').replace('\t','').replace(' ','')   #薪资
                item["wages"] = re.findall(".*-.*\d|.*\d",wagess.encode('utf-8'))[0]
                unit = re.findall(".*-.*\d(.*)|.*\d(.*)",wagess.encode('utf-8'))
                if unit:
                    if unit[0][0]:
                        item["unit"] = unit[0][0]
                else:
                    if unit[0][1]:
                        item["unit"] = unit[0][1]
            if companyName:
                item["companyName"] = companyName[0].extract()  #公司名称
            if nature:
                nature = nature[0].extract().replace('\r','').replace('\n','').replace('\t','').replace(' ','') #公司性质
                natures = nature.split('|')
                if natures[0]:
                    item["nature"] = natures[0]
                if natures[1]:
                    item["peonum"] = natures[1]
                if natures[2]:
                    item["industry"] = natures[2]

            if askFor:
                ask = askFor[0].extract().replace('\r', '').replace('\n', '').replace('\t', '').replace(' ', '')  # 工作经验
                # item["askFor"] =
                a = re.findall("(.*)年经验",ask.encode('utf-8'))
                if a:
                    item["askFor"] = a[0]
                else:
                    item["askFor"] = '0'

            if treatment:
                item["treatment"] = treatment[0].extract().replace('\r','').replace('\n','').replace('\t','').replace(' ','')   #待遇
            if minfo:
                infos = minfo[0].extract().replace('\r','').replace('\n','').replace('\t','').replace(' ','')   #工作要求
                item["minfo"] = re.findall("职位描述：(.*)分享微信邮件",infos.encode('utf-8'))[0]
            if contact:
                cont = contact[0].extract().replace('\r','').replace('\n','').replace('\t','').replace(' ','')   #公司地址
                item["contact"] = re.findall("地址：(.*)",cont.encode('utf-8'))[0].replace('地图','')
            item['url'] = response.url
            item['crawltime'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())) #抓取时间
        except Exception as e:
            print e
        return item







