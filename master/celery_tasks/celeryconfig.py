from celery.schedules import timedelta


BROKER_URL = 'redis://localhost:6379/5'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/4'
CELERY_TASK_SERIALIZER = 'msgpack'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_RESULT_EXPIRES = 60*60*24
CELERY_ACCEPT_CONTENT = ['json','msgpack']

CELERY_TIMEZONE = 'Asia/Shanghai'
CELERY_IGNORE_RESULT = True

CELERYBEAT_SCHEDULE = {
    'spider_scheduler':{
        'task':'celery_tasks.tasks.spider_scheduler',
        'schedule':timedelta(days=1)
    }
}
