from __future__ import absolute_import
from celery_tasks.app import app
from celery.task import periodic_task
from celery.schedules import timedelta
import os

# @app.task
# def add(x,y):
#     return x+y

# @periodic_task(run_every=timedelta(seconds=10))
# def quotesbot_spider_scheduler():
#     print 'Hello World !'

@app.task
def spider_scheduler():
    os.system('scrapy crawl jobspider')