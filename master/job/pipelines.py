# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from datetime import datetime
import settings
from items import JobItem
import redis

class JobPipeline(object):
    def __init__(self):
		self.rd = redis.Redis(host='localhost',port=6379,db=0)		

    def process_item(self, item, spider):
        if isinstance(item,JobItem):
            try:
                self.rd.rpush("jobspider:start_urls",item['url'])
            except Exception as e:
                print e
        return item
