# -*- coding: utf-8 -*-
import scrapy
from job.items import JobItem
from scrapy.http import Request
from scrapy_redis.spiders import RedisSpider
import time
import re
import redis

class JobspiderSpider(RedisSpider):
    name = 'jobspider'
    allowed_domains = ['search.51job.com','jobs.51job.com']
    redis_key = "jobspider:start_urls"
    start_urls = ['Python','Html','C%2523','Java','C%252B%252B','php','Android','iOS','Linux','javascript']

    

    def start_requests(self):
        for job in self.start_urls:
            url = 'http://search.51job.com/list/040000,000000,0000,00,2,99,{0},2,1.html?lang=c&stype=1&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&lonlat=0%2C0&radius=-1&ord_field=0&confirmdate=9&fromType=5&dibiaoid=0&address=&line=&specialarea=00&from=&welfare='.format(job)
            yield Request(url=url,callback=self.parse)

    def parse(self, response):
        rd = redis.Redis(host='localhost',port=6379,db=0)
        subSelector = response.xpath('//div[@id="resultList"]')
        for sub in subSelector:
            positionurls = sub.xpath("//div[@class='el']/p/span/a/@href").extract()
            for positionurl in positionurls:
                print positionurl
                rd.rpush("jobspider:start_urls",positionurl)

        nextpageurl = response.xpath('//div[@id="resultList"]/div[@class="dw_page"]/div[@class="p_box"]/div[@class="p_wp"]/div[@class="p_in"]/ul/li[@class="bk"]')
        nextpage = nextpageurl[1].xpath('./a/@href')[0].extract()
        print 'nextpageurl:%s'%nextpage
        yield scrapy.Request(nextpage.encode('utf-8'),callback=self.parse)
