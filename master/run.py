from scrapy import cmdline
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from job.spiders import jobspider

settings = get_project_settings()
process = CrawlerProcess(settings=settings)
process.crawl(jobspider.JobspiderSpider)

process.start()
